from django.db import models

categories = (("gaming_keyboards", "Gaming keyboards"),
              ("bicycles", "Bicycles"),
              ("medicines", "Medicines"),
              ("jigsaws", "Jigsaws"),
              ("shampoos", "Shampoos"))


class Goods(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(height_field=None, width_field=None, max_length=100,
                              null=True, blank=True, upload_to="items_images/")

    category = models.CharField(choices=categories, max_length=20, default="medicines")
    description = models.TextField()
    price = models.IntegerField()
