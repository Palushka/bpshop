from mixer.backend.django import mixer
from shop.models import Goods
from register.models import User
import pytest


@pytest.mark.django_db
class TestModel:

    def test_item_price_value(self):
        item = Goods.objects.get(id=69)
        assert item.price == 899

    def test_item_category_value(self):
        item = Goods.objects.get(id=72)
        assert item.category == "bicycles"

    def test_user_group(self):
        user = User.objects.get(username="Moderator")
        user_groups = []
        for i in user.groups.all():
            user_groups.append(i.name)
        assert "moderator" in user_groups

    def test_superuser_status(self):
        user = User.objects.get(username="Palushka")
        assert user.is_superuser
