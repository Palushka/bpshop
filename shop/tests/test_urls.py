from django.urls import reverse, resolve


class TestUrls:

    def test_item_url(self):
        path = reverse('shop-show-item', kwargs={'id': 69})
        assert resolve(path).view_name == 'shop-show-item'

    def test_basket_url(self):
        path = reverse('shop-user-basket')
        assert resolve(path).view_name == 'shop-user-basket'
