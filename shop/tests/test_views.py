from django.test import RequestFactory
from django.urls import reverse, resolve
from django.contrib.auth.models import User, AnonymousUser
import shop.views as view
import pytest


@pytest.mark.django_db
class TestViews:
    pass

    def test_shop_home(self):
        path = reverse('shop-home')
        request = RequestFactory().get(path)

        response = view.home(request)
        assert response.status_code == 200

    def test_shop_account_authenticated(self):
        path = reverse('shop-my-account')
        request = RequestFactory().get(path)
        request.user = User.objects.get(username="Palushka")

        response = view.my_account(request)
        assert response.status_code == 200

    def test_user_basket_unauthenticated(self):
        path = reverse('shop-user-basket')
        request = RequestFactory().get(path)
        request.user = AnonymousUser()

        response = view.show_basket(request)
        assert "/login" in response.url