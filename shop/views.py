from django.shortcuts import render, redirect
from shop.models import Goods
from .forms import CreateNewItem, PurchaseItem
from django.contrib.auth.decorators import login_required
from .decorators import group_required
from django.contrib.auth.models import User, Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from register.models import Profile
from django.contrib import messages


def home(request, category=""):
    items = Goods.objects.all().order_by("id")
    if category:
        items = Goods.objects.filter(category=category)
        return render(request, "shop/index.html", {"items": items})
    return render(request, "shop/index.html", {"items": items})


def show_item(request, id):
    item = Goods.objects.get(id=id)
    return render(request, "shop/show_item.html", {"item": item})


@login_required
@group_required(allowed_roles=["admin"])
def create(request):
    if request.method == "POST":
        form = CreateNewItem(request.POST, request.FILES)
        if form.is_valid():
            name = form.cleaned_data["name"]
            category = form.cleaned_data["category"]
            description = form.cleaned_data["description"]
            price = form.cleaned_data["price"]
            image = form.cleaned_data["image"]
            Goods.objects.create(name=name, category=category, image=image, description=description, price=price)
            messages.success(request, "Item created")
            return redirect("/create")
    form = CreateNewItem()
    return render(request, "shop/create.html", {"form": form})


@login_required
@group_required(allowed_roles=["admin", "moderator"])
def edit(request):
    items = Goods.objects.all().order_by("id")
    return render(request, "shop/edit_items.html", {"items": items})


@login_required
@group_required(allowed_roles=["admin", "moderator"])
def edit_item(request, id):
    item = Goods.objects.get(id=id)
    form = CreateNewItem(initial={"name": item.name, "category": item.category,
                                  "description": item.description, "price": item.price, "image": item.image})

    request.FILES['image'] = item.image

    if request.method == "POST":
        form = CreateNewItem(request.POST, request.FILES)

        if form.is_valid():
            name = form.cleaned_data["name"]
            category = form.cleaned_data["category"]
            description = form.cleaned_data["description"]
            price = form.cleaned_data["price"]
            image = form.cleaned_data["image"]
            item.name = name
            item.category = category
            item.description = description
            item.price = price
            item.image = image
            item.save()
            messages.success(request, "Saved")

        return redirect("/edit")

    return render(request, "shop/edit_item.html", {"item": item, "form": form})


@login_required
@group_required(allowed_roles=["admin"])
def delete_item(request, id):
    Goods.objects.get(id=id).delete()
    messages.error(request, "Item deleted")
    return redirect("/edit")


@login_required
def my_account(request):
    return render(request, "shop/my_account.html")


@login_required
def add_to_basket(request, id):
    username = request.user.username
    user = User.objects.get(username=username)
    user_basket = user.profile.basket
    if None in user_basket:
        user_basket = []
    user_basket.append(id)
    user.profile.basket = user_basket
    user.profile.save()
    return redirect("/basket")


@login_required
def show_basket(request):
    username = request.user.username
    user = User.objects.get(username=username)
    basket = user.profile.basket
    items = Goods.objects.filter(id__in=basket)
    return render(request, "shop/basket.html", {"items": items})


@login_required
def delete_from_basket(request, id):
    username = request.user.username
    user = User.objects.get(username=username)
    user_basket = user.profile.basket
    user_basket.remove(id)
    user.profile.basket = user_basket
    user.profile.save()
    return redirect("/basket")


@login_required
def purchase_item(request, id):
    form = PurchaseItem()
    if request.method == "POST":
        form = PurchaseItem(request.POST)
        if form.is_valid():
            messages.success(request, "You have successfully purchased")
            delete_from_basket(request, id)
            return redirect("/basket")
    return render(request, "shop/purchase_item.html", {"form": form})


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        instance.groups.add(Group.objects.get(name='user'))
        Profile.objects.create(user=instance, basket=[])
