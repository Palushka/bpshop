from .models import categories


def show_categories(request):
    return {'categories': categories}
