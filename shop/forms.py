from django import forms
from shop.models import categories


class CreateNewItem(forms.Form):

    name = forms.CharField(label="Name", max_length=100, widget=forms.TextInput(attrs={"autofocus": ""}))
    category = forms.CharField(label="Category", widget=forms.Select(choices=categories))
    description = forms.CharField(widget=forms.Textarea, label="Description", max_length=2000)
    price = forms.IntegerField(label="Price", max_value=1000000)
    image = forms.ImageField(label="Image")


class PurchaseItem(forms.Form):
    card_number = forms.CharField(label="Card number")
    first_name = forms.CharField(label="First Name", max_length=100)
    last_name = forms.CharField(label="Last Name", max_length=100)