import time
from locust import HttpUser, task, between


class QuickstartUser(HttpUser):
    wait_time = between(1, 2)

    @task
    def home_page(self):
        self.client.get("", name="index")

    @task
    def category_bicycles(self):
        self.client.get("items/bicycles", name="category bicycles")

    @task
    def category_shampoo(self):
        self.client.get("items/shampoos", name="category shampoos")

    @task
    def category_medicine(self):
        self.client.get("items/medicines", name="category medicines")

    @task
    def show_item(self):
        self.client.get("item/72", name="show item")

    @task
    def login(self):
        self.client.get("login", name="login")

    @task
    def account(self):
        self.client.get("account", name="account")

    @task
    def add_to_basket(self):
        self.client.get("add_to_basket/75", name="add item to basket")

    @task
    def register(self):
        self.client.get("register", name="register")

    @task
    def purchase_item(self):
        data = {
            "card number": 123,
            "first name": "Pavlo",
            "last name": "Andrushkiv",
        }
        self.client.post("purchase_item/69", data)



